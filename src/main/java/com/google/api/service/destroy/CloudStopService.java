package com.google.api.service.destroy;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.Operation;

/**
 * Created by Admin on 6/15/2017.
 */
public class CloudStopService {
public static void main(String args[]) throws IOException, GeneralSecurityException {
        // Project ID for this request.
//        String project = ""; // TODO: Update placeholder value.

        // The name of the zone for this request.
//        String zone = ""; // TODO: Update placeholder value.

        // Name of the instance resource to stop.
//        String instance = ""; // TODO: Update placeholder value.
	  /** Set PROJECT_ID to your Project ID from the Overview pane in the Developers console. */
	  final String project = "lively-epsilon-170708";

	  /** Set Compute Engine zone. */
	  final String zone = "asia-southeast1-a";

	  /** Set the name of the sample VM instance to be created. */
	  final String instance = "my-sample-instance";


        Compute computeService = createComputeService();
//        Compute.Instances.Stop request = computeService.instances().stop(project, zone, instance);
//
//        Operation response = request.execute();

        Compute.Instances.Stop request = computeService.instances().stop(project, zone, instance);

        Operation response = request.execute();

        // TODO: Change code below to process the `response` object:
        System.out.println(response);
        }

public static Compute createComputeService() throws IOException, GeneralSecurityException {
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

        GoogleCredential credential = GoogleCredential.getApplicationDefault();
        if (credential.createScopedRequired()) {
        credential =
        credential.createScoped(Arrays.asList("https://www.googleapis.com/auth/cloud-platform"));
        }

        return new Compute.Builder(httpTransport, jsonFactory, credential)
        .setApplicationName("Google-ComputeSample/0.1")
        .build();
        }
        }
